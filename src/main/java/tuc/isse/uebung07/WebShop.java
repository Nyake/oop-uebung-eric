package tuc.isse.uebung07;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

public class WebShop {	
	
	public List<String> itemNames = new Vector<String>();
	public List<Integer> itemPrice = new Vector<Integer>();
	
	public List<String> customerNames = new Vector<String>(); 	
	public Map<String,List<Integer>> cartItems = new HashMap<String,List<Integer>>();
	
	
	public List<String> urls = new Vector<String>();
	
	public String getItemName(int itemIndex) {
		return itemNames.get(itemIndex);
	}
	
	public void setItemName(int itemIndex, String newName) {
		itemNames.set(itemIndex, newName);
	}
	
	public int getItemPrice(int itemIndex) {
		return itemPrice.get(itemIndex);
	}
	
	public void setItemPrice(int itemIndex, int newPrice) {
		itemPrice.set(itemIndex, newPrice);
	}
	
	public Integer addItem(String name, int price) {
		itemNames.add(name);
		itemPrice.add(price);
		return itemNames.size();
	}
	
	public void printItem(int itemIndex) {
		System.out.println("item " + itemIndex + ":" + itemNames.get(itemIndex) + " Price:" + itemPrice.get(itemIndex));
	}	

	
	public void addCustomer(String name) {
		customerNames.add(name);
		cartItems.put(name, new Vector<Integer>());		
	}
	
	public void removeCustomer(String name) {
		customerNames.remove(name);
		List<Integer> cart = cartItems.get(name);
		cartItems.remove(cart);
	}
	
	public void printCustomer() {
		for (String name : customerNames) {
			System.out.println("customer:" + name);
		}
	}
	
	public void addItemToCart(String name, int itemIndex) {
		cartItems.get(name).add(itemIndex);
	}
	
	public void addURL(String url) {
		urls.add(url);
	}
	
	public void removeURL(String url) {
		urls.remove(url);
	}
	
	public void printURL() {
		for (String url : urls) {
			System.out.println(url);
		}
	}
	
}
